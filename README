LTTNG USERSPACE TRACER (UST)
----------------------------

UST web site and manual: http://lttng.org/ust

Updated versions of this package may be found at:

  * Website:  http://lttng.org/ust
  * Releases: http://lttng.org/files/ust/releases
  * GitWeb:   http://git.lttng.org (project: ust)
  * Git:      git://git.lttng.org/ust.git


PREREQUISITES:

  - liburcu
    Userspace RCU library, by Mathieu Desnoyers and Paul E. McKenney

    -> This release depends on liburcu v0.6.6

      * Debian/Ubuntu package: liburcu-dev
      * Website:  http://lttng.org/urcu
      * Releases: http://lttng.org/files/urcu
      * GitWeb:   http://lttng.org/cgi-bin/gitweb.cgi?p=userspace-rcu.git;a=summary
      * Git:      git://lttng.org/userspace-rcu.git


INSTALLATION INSTRUCTIONS:

  - Download, compile and install liburcu.
  - In this package's tree, run ./configure.
  - Run make.
  - Run make install.
  - Run ldconfig.
  - See the manual for usage instructions.

  If compiling from the git repository, run ./bootstrap before running
  the configure script, to generate it.


TRACE VIEWER:

  LTTV is used for viewing UST traces. LTTV may be obtained at
  http://lttng.org in the Downloads section.

  This release has been tested with LTTV 0.12.32.


CONTACT:

  Maintainer: Mathieu Desnoyers <mathieu.desnoyers@efficios.com>
  Mailing list: ltt-dev@lists.casi.polymtl.ca


PACKAGE CONTENTS:

  This package contains the following elements.

  - libust
    The actual userspace tracing library that must be linked to the
    instrumented programs.

  - ustctl
    A program to control the tracing of userspace applications. It can list
    markers, start the tracing, stop the tracing, enable/disable markers, etc.

  - include
    The public header files that will be installed on the system.

  - ust-consumerd
    The daemon that collects trace data and writes it to the disk.

  - doc
    The documentation.

  - tests
    Various test programs

  - libustinstr-malloc
    An example library that can be LD_PRELOAD'ed to instrument calls to malloc()
    in any program without need to recompile it.

  - libustfork
    A library that is LD_PRELOAD'ed, and that hijacks calls to several system
    calls in order to trace across these calls. It _has_ to be LD_PRELOAD'ed
    in order to hijack calls. In contrast, libust may be linked at build time.

  - libustctl
    A library to control tracing in other processes. Used by ustctl.

  - libustcomm
    A static library shared between libust, ust-consumerd and libustctl, that
    provides functions that allow these components to communicate together.

  - libustconsumer
    A library to create ust consumers by registering callbacks, used by
    ust-consumerd.

  - snprintf
    An asynchronous signal-safe version of snprintf.

  - java
    A simple library that uses JNI to allow tracing in java programs.
