/*
 * Copyright (C) 2009 Novell Inc.
 *
 * Author: Jan Blunck <jblunck@suse.de>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License version 2.1 as
 * published by the Free  Software Foundation.
 */

#include <ust/marker.h>
#include <ust/tracepoint.h>

UST_MARKER_LIB;
TRACEPOINT_LIB;
TRACEPOINT_EVENT_LIB;
