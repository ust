usttrace(1) -- the simplest way to record a trace
=================================================

## SYNOPSIS

`usttrace` [<options>] [<command>]

## DESCRIPTION

The `usttrace` script automatically:

* creates a daemon
* enables all markers
* runs the command specified on the command line
* after the command ends, prints the location where the trace was saved

Each subdirectory of the save location contains the trace of one process that
was generated by the command. The name of a subdirectory consists in the PID of
the process, followed by the timestamp of its creation.

The save location also contains logs of the tracing.

When using usttrace, the early tracing is always active, which means that the
tracing is guaranteed to be started by the time the process enters its main()
function.

Several usttrace's may be run simultaneously without risk of conflict. This
facilitates the use of the tracer by independent users on a system. Each
instance of usttrace starts its own daemon which collects the events of the
processes it creates.

## OPTIONS

These programs follow the usual GNU command line syntax, with long options
starting with two dashes(`-'). A summary of options is included below.

  * `-h`:
    Show summary of options.

  * `-l`:
    Runtime link with UST library. (Needed only if program was not linked at
    compile time with libust.)

  * `-L`:
    Add path to ust libraries to LD_LIBRARY_PATH.

  * `-m`:
    Instrument malloc calls.

  * `-f`:
    Also trace forked processes.

  * `-s`:
    Use system-wide daemon instead of creating one for this session.

  * `-S`:
    Specify the subbuffer size.

  * `-N`:
    Specify the number of subbuffers.

  * `-W`:
    Print the location of the last trace saved in the usttrace output directory.

## SEE ALSO

ustctl(1), ust-consumerd(1)


## AUTHOR

`usttrace` was written by Pierre-Marc Fournier.

This manual page was written by Jon Bernard &lt;jbernard@debian.org&gt;, for
the Debian project (and may be used by others).
