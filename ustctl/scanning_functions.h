/* Copyright (C) 2011  Ericsson AB, Nils Carlson <nils.carlson@ericsson.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
 */
#ifndef __SCANNING_FUNCTIONS_H
#define __SCANNING_FUNCTIONS_H

int parse_and_connect_pid(const char *pid_string);

int scan_ch_marker(const char *channel_marker, char **channel, char **marker);

int scan_ch_and_num(const char *ch_num, char **channel, unsigned int *num);

#endif /* __SCANNING_FUNCTIONS_H */
