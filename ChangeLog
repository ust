2011-12-15 ust 0.16
	* Use caa_unlikely, depend on liburcu >= 0.6.6
	* usttrace: allow spaces in command-line arguments
	* Fix tracepoint test with deprecated API
	* Add powerpc64 host_cpu to configure.ac
	* Mark __tp_cb_data as possibly unused for backward compat API
	* Fix git URL

2011-07-15 ust 0.15
	* Add backward compability for tracepoint API (still planned for
	  deprecation, but should make the transition smoother).

2011-06-12 ust 0.14
	* Rename trace_mark() API to ust_marker(). Old macros kept for
	  slow deprecation.
	* Tracepoint API is now private to UST.
	* Introduction of the TRACEPOINT_EVENT API (work in progress).
	  Please use ust_marker() API while we complete TRACEPOINT_EVENT.
	* Privatize many headers which should have always been internal
	  to UST. At the moment, only marker.h should be included by
	  applications. When the TRACEPOINT_EVENT API will be completed,
	  then other headers will be useful for the application
	  (tracepoint_event.h, tracepoint.h and, indirectly, probe.h).
	* Markers: merge fixes from LTTng 0.249 into marker.[ch].
	* Add ust/version.h header file, which exports the UST version
	  installed.
	* Move to liburcu 0.6. Update dependencies.

2011-05-19 ust 0.13
	* API CHANGE!!! trace_mark has been deprecated, new ust_maker, without
	channel name. ex. ust_marker(name, <format>, args...)
	* Instrumentation API CHANGE!!! change from trace_<name>(args...) to
	tracepoint(name, args...), register_trace_<name>(...) to
	register_tracepoint(name, ...) and unregister_trace_<name>(...) to
	unregister_tracepoint(name, ...)
	* Instrumentation API CHANGE!!! TRACE_EVENT -> TRACEPOINT_EVENT etc
	* Mutliple ust-consumerd fixes
	* Fix a bug in the tracepoint ptrs section
	* Clarify licenses
	* Busybox support in most scripts added
	* Remove some dead code
	* List only online pids and pids that can be accessed by current users
	using libustctl

2011-03-14 ust 0.12
	* ARM support
	* Fix lots of low-level bugs, mostly courtesy of Mathieu and Yannick
	* Introduce new ustctl cli
	* API CHANGE!!! libustcmd renamed to libustctl
	* API CHANGE!!! libustctl now uses sockets instead of pids
	* Short signal names in scripts for busybox compatibility

2011-01-14 ust 0.11
	* Fix a serious bug in the newly introduced synchronized timer
	implementation.
	* Increase compatibility of an autoconf test

2011-01-14 ust 0.10
	* Run time check for lttng clock source providing synchronized
	timestamps
	* Rename of ustd and libustd to ust-consumerd and libustconsumer,
	remember to run ldconfig

2010-12-03 ust 0.9
	* Cleaned up namespace
	* Compatible with userspace-rcu version 0.5.2
	* Fixed a minor bug in a test

2010-10-26 ust 0.8
	* API CHANGE!!! Add data pointer to tracepoint probe
	void *data is always the first argument to a tracepoint probe
	* License LGPGv2.1 text and information added
	* Update COPYING file
	* Add TRACE_EVENT api proposal, not finalised
	NOT usable at this point. Only bound to a printf.
	ustctl command for listing trace event
	* Multiple fixes
	Compilation warnings
	Value and error handling
    	Fix bugs introduce by prior commits
	Security fix (CVE-2010-3386)
	Add exit status for ustctl
	Change malloc to zmalloc for most of UST
	Remove unused code, syntax cleanup and consistency for markers
	* New UST internal communication protocol
	Add message headers
	Usage of epoll
	* New testcases
	* Try to decrease namespace pollution
	* Fix valgrind for sendmsg bug
	* tracectl destructor aware of fork

2010-08-16  ust 0.6
	* add ust strncpy interface for input robustness
	* multiple memory barrier fix
	* add libustd: contains the core functionality of ustd
	* add get/set commands for daemon socket path
	* fix cacheline aligned to use liburcu value
	* fix usage of custom probes: use trace_mark_tp
	* remove ltt_relay_buffer_flush function
	* add ustctl manpage: force-switch and get/set-sock-path
	* fix invalid array size and directory creation mode for ustd

2010-06-30  ust 0.5
	* add support for PowerPC 32 architecture
	* use clock_gettime(CLOCK_MONOTINIC) instead of gettimeofday()
	* add several tests
	* output test suite results in TAP format
	* add support for traced library unregistration (useful with dlclose())
	* add manpages
	* compile with -fno-strict-antialiasing
	* add support for channel overwrite and non-collection
	* update tracepoint api from kernel
	* fix various bugs and do various cleanups
	* improve documentation

2010-03-17  ust 0.4
	* remove dependency on libkcompat
	* use atomic ops from liburcu
	* libmallocwrap: rename to libustinstr-malloc
	* libinterfork: rename to libustfork
	* add a testsuite
	* add ustctl --get-subbuf-size and --get-subbuf-num
	* updates for compilation with gcc-4.4
	* remove periodical wakeup in listener thread
	* change benign warnings to debug messages, not to confuse users
	* ustd: fix destruction to free all file descriptors and memory
	* libust: close all file descriptors on close
	* remove periodical wakeup in daemon
	* code cleanups

ust 0.3
	* fix several issues, stabilizing tracing of large applications
	* add --set-subbuf-size / --set-subbuf-num options to ustctl
	* add a signal safe implementation of snprintf

ust 0.2
	* conversion of the buffering system to per-cpu
	* update to the liburcu 0.4 API
	* upgrade to trace format 2.6; the latest lttv may now be used
	* add support for clone() interception in libinterfork
	* port refactorings from the kernel tracer up to 0.173
	* enhancements/refactor of libustcomm
	* add sections in the manual about trace viewing and about tracing apps not linked to libust
	* debugging output is now enabled by adding -DUST_DEBUG to the CFLAGS
	* usttrace: change ustd socket name to ustd-sock-PID
	* change app socket directory name for something more descriptive (/tmp/ust-app-socks)
	* code cleanups
	* various bug fixes
